require 'spec_helper'

two_jobs_json = {'jobs'=>[{'name'=>'job1','url'=>'http://jenkins/job1'}, {'name'=>'job2','url'=>'http://jenkins/job2'}] }
describe Terkins::View do

  let(:terkins2jobs) { Terkins::View.new(two_jobs_json) }

  it 'should list all jobs in a view' do
    jobs = terkins2jobs.list_jobs
    expect(jobs.size).to eq(2)
    names = jobs.map { |j| j['name'] }
    expect(names).to include('job1').and include('job2')
  end

  it 'should filter job by name' do
    filtered_jobs = terkins2jobs.filter('b1')
    expect(filtered_jobs.size).to eq(1)
    names = filtered_jobs.map { |j| j['name'] }
    expect(names).to include('job1')
  end

  it 'should return no job when filter does not match' do
    filtered_jobs = terkins2jobs.filter('linux')
    expect(filtered_jobs.size).to eq(0)
  end

end
