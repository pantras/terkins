require_relative '../lib/terkins'

def format(job)
  puts "#{job['name']}\t#{job['color']}"
end

begin
  terkins = Terkins::Terkins.new
rescue => error
  puts error
  exit 1
end
jobs = terkins.call(ARGV.first)
jobs.each { |j| format(j) }
