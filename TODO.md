# terkins - terminal Jenkins - "streetmap"

## Must
[x] ISBAT list all jobs
[x] ISBAT filter jobs via command-line parameters
[ ] ISBAT configure jenkins URI
[ ] ISBAT get entire job description

## Should 
[ ] ISBAT configure URIs
[ ] ISBAT get a quick error message when Jenkins is unreachable
[ ] ISBAT install terkins as a Ruby gem
[ ] ISBAT download terkins from rubygems.org

## Could
[ ] ISBAT configure aliases for views
[ ] ISBAT generate my conf file from a jenkins URI

# Won't

# Usage
```
jenkins-text integration 3.9
jenkins-text regression 3.9 linux
jenkins-text regression 3.9 linux
```

### jenkins-test.yml
```yml
default:
views:
 - integration3.9: http://....
 - regression3.9: http://....
   - alias: regression39 reg3.9 reg39
   - default_filter:  linux
