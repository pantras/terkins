require 'json'
require 'rest-client'

require_relative "terkins/version"

module Terkins
class View
  def initialize(json)
    @view = json
  end
  def list_jobs
    @view['jobs']
  end
  def filter(filter)
    list_jobs.select { |job| job['name'].match /#{filter}/ }
  end
end

class Terkins
  def initialize(url = 'http://localhost:8080/api/json')
    begin
    response = RestClient::Request.execute(method: :get, url: url)
    @view = View.new(JSON.parse(response))
    rescue => x
      raise "#{url}: #{x}"
    end
  end
  def call(filter)
    @view.filter(filter)
  end
end
end
